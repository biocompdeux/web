# README #

This is the only git repository for group chromosome 3. It includes all code, scripts etc. for all layers.

![ntier2.png](https://bitbucket.org/repo/Kpbnez/images/4168577113-ntier2.png)

Notes:

### How do I get set up? ###

* Summary of set up

-- Clone the git project to a directory in your student account. eg:
```
mkdir -p ~/biocomp/project
cd ~/biocomp/project
git clone git@bitbucket.org:biocompdeux/web.git
```

-- Make the deploy script executable
```
chmod +x deploy.sh
```
-- Edit the config file in cgi-bin/data
```
cd cgi-bin/data
cat config.ini
#config.ini
#This file is included at runtime by the datalayer package,
#but is isolated here to allow distributions to modify these values easily.
#the left hand side variable will be converted to lowercase
#
# Example values that are required to be set in this file:
# driver=mysql
# hostname=localhost
# database=ad001
# user=ad001
# password=myspecialpassword
#
driver=mysql
hostname=localhost
database=ad001
user=ad001
password=mypassword
```
-- You can test the config in the following way (It will not mask output of your password)
```
./testconfig.pl
```
-- If database schema already exists, you can drop all tables in the target database with this script
```
./dropalltables.sh
```
-- Setup the schema via windows batch
```
schema
```
--Setup the schema via unix shell
```
./schema.sh
```
--Extract data from the the schema via unix shell
--It requires a single argument of either a zipped or unzipped chromosome file
```
./datalayer.pl
This script, ./datalayer.pl requires 1 command-line arguments, and you supplied 0
Usage:
         ./datalayer.pl <ChromosomeFile>

```
-- Finally, run the deployment script
```
./deploy.sh
```

# Database schema #

![schema.png](https://bitbucket.org/repo/Kpbnez/images/287556582-schema.png)

* How to run tests 
Tests have been written for the /app module.

```
cd cgi-bin/app
prove
```

* 


### Contribution guidelines ###

* Ensure test passes
* Code review
* Follow coding convention