#!/bin/bash
echo "Deployment script for BiocomputingDeux Project"
echo "=============================================="

cp cgi-bin/*.pl ~/WWW/cgi-bin
mkdir -p ~/WWW/cgi-bin/app
cp cgi-bin/app/*.pm ~/WWW/cgi-bin/app
mkdir -p ~/WWW/cgi-bin/web
cp cgi-bin/web/*.pm ~/WWW/cgi-bin/web
mkdir -p ~/WWW/cgi-bin/data
cp cgi-bin/data/*.pm ~/WWW/cgi-bin/data
cp cgi-bin/data/config.ini ~/WWW/cgi-bin/data
mkdir -p ~/WWW/css
cp css/*.css ~/WWW/css
#mkdir -p ~/WWW/data
#cp data/*.txt ~/WWW/data

chmod +x ~/WWW/cgi-bin/*.pl
chmod +x ~/WWW/cgi-bin/app/*.pm
chmod +x ~/WWW/cgi-bin/web/*.pm
chmod +x ~/WWW/cgi-bin/data/*.pm
chmod +r ~/WWW/cgi-bin/data/*
chmod +r ~/WWW/data/*.txt

echo "Run schema.sh to populate database schema"
