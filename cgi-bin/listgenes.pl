#!/usr/bin/perl
# listgene.pl ## displays a summary of all genes for Chromosome 3

use warnings;
use CGI;
use strict;
use Data::Dumper;
use CGI::Carp qw/fatalsToBrowser/;
use lib qw(app web data);
use Display;

my $search;
my $cgi = new CGI;
$search = $cgi->param( 'search' );
my $genepart = $cgi->param('genepart');
my %genes = Display::ListGenes($search,$genepart);

######## feedback to user on search results #####################
my $lead = "";
if ($search =~ /^\s*$/) {
  $lead = "Showing all genes from Chromosome 3";
} else {
  $lead = "You have searched for: \"$search\" within $genepart";
}

####### display table of genes ##################################
my $geneTable = Display::GetGeneTable(%genes);

## establish environment for different users within our group ###
my $userName = $ENV{LOGNAME} || $ENV{USER} || getpwuid($<);


#### HTML5 code using Bootstrap ##################################
print $cgi->header(-charset=>'utf-8');

print <<__EOF;
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ListGenes</title>

  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href="/~$userName/css/custom.css" rel="stylesheet">

</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="listgenes.pl">Chromosome 3</a>
      </div><!-- class="navbar-header" -->
      <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right" action="listgenes.pl">
          <div class="form-group">
	    <select class="form-control" name = "genepart" size = "1">
	      <option value="geneid">Gene ID</option>
	      <option value="accession">Accession</option>
	      <option value="location">Location</option>
	      <option value="product">Product</option>
	    </select>
            <input type="text" name="search" placeholder="Search for gene" class="form-control">
          </div><!-- class="form-group" -->
          <button type="submit" class="btn btn-info">Search</button>
        </form><!-- class="navbar-form navbar-right" -->
     </div><!--/."navbar-collapse" -->
   </div><!--class="container" -->
 </nav>


<!-- Begin page content -->
<div class="container">
  <div class="page-header">
    <h1>Chromosome 3</h1>
  </div><!-- class="page-header" -->
__EOF
print '<p class="lead">'.$lead.'</p>';
print $geneTable;
print <<__EOF;

</div><!-- class="container" -->
<footer class="footer">
  <div class="container">
    <p class="text-muted">MSc Bioinformatics, Birkbeck &mdash; &copy; Biocomputing II Team III, 2016</p>
  </div><!-- class="container" --> 
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
-->
</body>
</html>
__EOF
