package Display;
use strict;
use Exporter;
use Data::Dumper;
use List::Util 'sum';

use lib qw(../app);
use Genes;

sub GetGeneTable;
sub ShowCodingAndEnzymeRegion;
sub GraphCodingFrequencies;
sub ShowCodonFrequencies;


# ListGenes ==================================================================================
# this subroutine is a 'separation' step between listgenes.pl and
# Genes.pm.  listgenes.pl passes two scalar search terms the first is 
# search term input by the user and the second is a filter - geneid|
# product|location|accession.  These arguments are passed directly to 
# Display.pm that returns the matching set of genes

sub ListGenes($$) {
  my $search = $_[0]; # search term eg. 'ribsomal'
  my $filter = $_[1];	# filter by one of: [geneid|product|location|accession]
  return Genes::ListGenes($search, $filter);
}

# GetGen =====================================================================================
# this subroutine is a 'separation' step between getgene.pl and
# Genes.pm.  getgene.pl passes a scalar 'geneid' to Genes.pm which
# returns detailed information (dna sequence, aa sequence, location,
# product, codon frequencies)  

sub GetGene($) {
  my $geneid = $_[0];
  return Genes::GetGene($geneid);
}

# GetGeneTable ===============================================================================
# returns a string containing summary information for all or some of the 
# genes in Chromsome 3.  The string has HTML tags placed on it so that
# it can be displayed as a table within listgenes.pl.  The subroutine is
# called from listgenes.pl and is passed a hash containing either all the
# genes in Chromosome 3 or a filtered subset of them.  Consequently, 
# this subroutine can be use to show the initial list of all genes or 
# the result of search.  Each row of the table can be clicked on to provide
# futher details on that particular gene found in getgene.pl The addition 
# of the anchor tag '#dna' at the end of each href ensure that the getgene.pl
# loads in the exactly same part of the page each time.

sub GetGeneTable(\%) {
  my %allGenes = %{$_[0]};
  my $str = "";
  $str .= '<table class="table table-hover table-condensed">
    <thead>
      <tr>
        <th class="index">#</th>
        <th class="gi">GI</th>
        <th>Accession</th>
        <th class="location">Location</th>
        <th>Product</th>
      </tr>
    </thead>
    <tbody>';
  my $count = 1;
  foreach my $key (keys %allGenes) {
      $str .= "
      <tr>     
         <th scope='row' class='col-md-1'><a href='getgene.pl?gi=$key#dna'>$count</a></th> 
         <td class='col-md-2'><a href='getgene.pl?gi=$key#dna'>$key</a></td>
         <td class='col-md-3'><a href='getgene.pl?gi=$key#dna'>$allGenes{$key}{accession}&nbsp;</a></td>
         <td class='col-md-2'><a href='getgene.pl?gi=$key#dna'>$allGenes{$key}{location}&nbsp;</a></td>
         <td class='col-md-4'><a href='getgene.pl?gi=$key#dna'>$allGenes{$key}{product}&nbsp;</a></td>
      </tr>
	";
    $count++;
  }
  $str .= '    </tbody>
    </table>';
  return $str;
}

# ShowCodingAndEnzymeRegion ==================================================================
# getgene.pl passes, as a scalar string, the DNA region for a gene.
# The passed string contains lower and uppercase characters.  The uppercase
# characters represent coding regions of the gene. ShowCodingRegion places 
# <em><\em> around uppercase characters. 

sub ShowCodingAndEnzymeRegion($$$){

my $dnaStr = $_[0];  
my $preEnz = $_[1];
my $userEnz= $_[2];  
my $message;
my $endMessage;
my $enz;

# user search messages #################################

if ($preEnz eq "" && $userEnz eq""){
$message="No search entered";
$enz="";
}
if ($preEnz ne "" && $userEnz ne ""){
$message="<span class='error'>Cannot select an enzyme from the dropdown list and search for your own motif.  One or the other please!</span>";
$enz="";
}
if ($preEnz ne "" && $userEnz eq ""){
$message="You searched for the pre-set enzyme <span class='enzyme'>'$preEnz'.&nbsp;&nbsp;</span>";
$enz=$preEnz;
}
if ($preEnz eq "" && $userEnz ne ""){
$message="You searched for your own enzyme <span class='enzyme'>'$userEnz'.&nbsp;&nbsp;</span>";
$enz=$userEnz;
}
if ($enz =~ /[efijlopquxz]+/g ){
$message= "<span class = 'error'>Only nucleobases allowed are a,b,c,d,g,h,k,m,n,r,s,t,v,w,y.&nbsp;</span>";
$enz="";
}

# apply tags to enzyme hits ############################

my $span='<span class="enzyme">'; 
my $endspan='</span>';
my $found=0;
my $newDnaString="";

if ($enz ne ""){
    $found = $dnaStr =~ s/($enz)/$span$1$endspan/ig;
    }
if ($found eq ""){
	$found=0; # avoid returning a null string
	}  

# apply tags to coding (uppercase) characters #########

foreach my $char (split //, $dnaStr) {
    if ($char =~ /[A-Z]/g) {
        $newDnaString .= '<em>'.$char.'</em>';
       	} else {
      	$newDnaString .= $char;
    	}
  	}
  	
# user results messages #################################

if ($message eq "No search entered" || $enz eq "") {
	$endMessage="";
	}else{
	$endMessage="There were <span class='enzyme'>$found </span>binding site(s) found";
	}   	
  	
return ($newDnaString,$found,$message, $endMessage);
}


# ShowCodonFrequencies($) (by Chelsie Woon) ===============================================
sub ShowCodonFrequencies($) {
  my $geneid = $_[0];
  my %codonFreq = Genes::GetCodonFrequencies($geneid);
  my $geneCodonTotal = sum values %codonFreq;
  my %chromosomeCodonFreq = Genes::GetChromosomeCodonFrequencies();
  my $chromosomeCodonTotal = sum values %chromosomeCodonFreq;

  my $bases = "TCAG";
  my (@codonOrder, @geneCodonRatio, @chromosomeCodonRatio);
  foreach my $base1 (split("", $bases)) {
    foreach my $base2 (split("", $bases)) {
      foreach my $base3 (split("", $bases)) {
        my $codon = $base1.$base2.$base3;
        push(@codonOrder, '"'.$codon.'"');
        push(@geneCodonRatio, defined($codonFreq{$codon}) ? $codonFreq{$codon}/$geneCodonTotal : 0);
        push(@chromosomeCodonRatio, defined($chromosomeCodonFreq{$codon}) ? $chromosomeCodonFreq{$codon}/$chromosomeCodonTotal : 0);
      }
    }
  }

  my $str = '<script type="text/javascript">
    $(document).ready(function () {
    $("#container").highcharts({
    chart: {
            type: "column"
        },
        title: {
            text: "Codon Percentage Frequency"
        },
        subtitle: {
            text: "Gene vs Chromosome"
        },
        xAxis: {
            categories: ['.
                join(',', @codonOrder)
            .'],
            crosshair: true,
            labels: {
                rotation: -90
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: "frequency e.g. 0.04 = 4%"
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: "Gene",
            data: ['.
              join(',', @geneCodonRatio)
            .']

        }, {
            name: "Chromosome",
            data: ['.
              join(',', @chromosomeCodonRatio)
            .']

        }]
    });
});
    
  </script>';
  #ß$str .= Dumper(\@geneCodonRatio, \%codonFreq);

  return $str;
}

1;
