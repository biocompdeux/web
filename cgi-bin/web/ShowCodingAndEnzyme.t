use strict;
use warnings;

############ this is on getgene.pl
sub ShowCodingAndEnzymeRegion;


my $extractedDna = 'aaAGaaT';  
my $presetEnzyme='';
my $userEnzymeSearch='aaaa';

my @dna=ShowCodingAndEnzymeRegion($extractedDna, $presetEnzyme, $userEnzymeSearch);
my $taggedDna=$dna[0];
my $hitCount=$dna[1];
my $searchMessage=$dna[2];
my $resultsMessage=$dna[3];

print "tagged DNA = $taggedDna\n";
print "search message = $searchMessage\n";
print "results messge = $resultsMessage\n";
print "hits = $hitCount\n";
print "strings in use are raw dna = $extractedDna preset enzyme = $presetEnzyme user enzyme = $userEnzymeSearch\n";

################### this in Display.pm  #######################

sub ShowCodingAndEnzymeRegion($$$){

my $dnaStr = $_[0];  
my $preEnz = $_[1];
my $userEnz= $_[2];  
my $message;
my $endMessage;
my $enz;
########### user search messages ####################

if ($preEnz eq "" && $userEnz eq""){
$message="No search entered";
$enz="";
}
if ($preEnz ne "" && $userEnz ne ""){
$message="<span class='error'>Cannot select an enzyme from the dropdown list and search for your own motif.  One or the other please!</span>";
$enz="";
}
if ($preEnz ne "" && $userEnz eq ""){
$message="You searched for the pre-set enzyme <span class='enzyme'>'$presetEnzyme'.&nbsp;&nbsp;</span>";
$enz=$preEnz;
}
if ($preEnz eq "" && $userEnz ne ""){
$message="You searched for your own enzyme <span class='enzyme'>'$userEnzymeSearch'.&nbsp;&nbsp;</span>";
$enz=$userEnz;
}
if ($userEnz !~ /[abcdghkmnrstvwy]+/g){
	$message= "Only nucleobases allowed are a,b,c,d,g,h,k,m,n,r,s,t,v,w,y";
}

############ applying tags to enzyme hits and coding sections of DNA ################

my $span='<span class="enzyme">';
my $endspan='</span>';
my $found=0;
my $newDnaString="";

### apply tags around enzyme hits - an enzyme has been searched for ################
### otherwise go to next section and apply tags around coding regions #############

if ($enz ne ""){
    $found = $dnaStr =~ s/($enz)/$span$1$endspan/ig;
    }
if ($found eq ""){
	$found=0;
	}  ## avoid returning a null string

###### search enzyme 'tagged' DNA string for upper and lower case characters #########

foreach my $char (split //, $dnaStr) {
    if ($char =~ /[A-Z]/g) {
        $newDnaString .= '<mark>'.$char.'</mark>';
       	} else {
      	$newDnaString .= $char;
    	}
  	}
  	
############## user results messages ####################################################
if ($message ne "No search entered" || $enz eq "") {
		$endMessage= "There were <span class='enzyme'>$found </span>binding site(s) found";
	}   	
  	
return ($newDnaString,$found,$message, $endMessage);
}
