#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use File::Spec;
use lib File::Spec->catdir($FindBin::Bin, '..', '../data');

use Test::More;
use Genes;

use Data::Dumper;


require_ok("Genes");

# redefine subroutine to load mock data
sub GeneData::GetGenes() {
  my $geneStr = 'GI:111111|protein product 1|3q1|AB000001
GI:2222222|protein product 2||AB000002
GI:3333333||3q2|AB0000003
GI:4444444|PROTEIN product 4|3q4.33|
GI:5555555|protein product 5|3q4-3q4.1|AB000004
GI:6666666|protein product 6|3p1|AB000005 AB000006 
GI:7777777|protein product 7|3p2|AB000007 
';
  my @genelist = split/\n/, $geneStr;
  my %genes = GeneData::parseGenes(\@genelist);
  return %genes;
}

# Test that list genes returns mock data
sub testListGenes() {
  my %genes = Genes::ListGenes();
  note(Dumper(\%genes));
  my $count = scalar (keys %genes);
  return $count;
}

is ( testListGenes(), 7, "ListGenes with no params should contain 7 rows");

# Test filter and search parameters in listgene. user provides search terms and filter
sub testListGenesWithFilter($$) {
  my $searchword = $_[0];
  
  my $filter = $_[1];
  my %genes = Genes::ListGenes($searchword,$filter);
  return scalar (keys %genes);
}

is ( testListGenesWithFilter("GI:111111", "geneid"), 1, "Search for 'geneid=GI:111111' should return 1 result");  #exact geneid search
is ( testListGenesWithFilter("111111", "geneid"), 1, "Search for 'geneid=111111' should return 1 result");  #partial geneid search
is ( testListGenesWithFilter("gi:111111", "geneid"), 1, "Search for 'geneid=gi:111111' should return 1 result");  #case insensitive geneid search
is ( testListGenesWithFilter("", "geneid"), 7, "Search for 'geneid=' should return 7 results"); #list all genes
is ( testListGenesWithFilter("protein product", "product"), 6, "Search for 'product=protein product' should return 6 results"); #search with full product name
is ( testListGenesWithFilter("pro", "product"), 6, "Partial Search for 'product=pro' should return 6 results"); #search with partial product name
is ( testListGenesWithFilter("AB000001", "accession"), 1, "Search for 'accession=AB000001' should return 1 result"); #search gene with one accession
is ( testListGenesWithFilter("AB000005", "accession"), 1, "Search for 'accession=AB000005' should return 1 result"); #gene with multiple accession 
is ( testListGenesWithFilter("3q4", "location"), 2, "Search for 'location=3q4' should return 2 result"); #exact location search
is ( testListGenesWithFilter("3q", "location"), 4, "Search for 'location=3q' should return 4 result"); # partial location search 



# test getgene returns correct details of the gene. eg: Expect to see same accession
my %test= Genes::GetGene("GI:111111"); # retrieve a gene 
my $expectedaccession = "AB000001";
my $actualaccession = $test{accession};
is ($actualaccession,$expectedaccession,"Your expected and actual accession match"); #compare expected and actual accession


# test that codon count is what we expect
sub testCodonFrequencies($$$) {
  my $dna = $_[0];
  my $codonToTest = $_[1];
  my $expectedCount = $_[2];
  my %freq = Genes::GetCodonFrequencies($dna);
  note(Dumper(\%freq));
  if (exists($freq{uc($codonToTest)}) && $freq{uc($codonToTest)} == $expectedCount) {
    return 1;
  }
  return 0;
}

ok (testCodonFrequencies("uuuuuu", "uuu", 2));
ok (testCodonFrequencies("uuucccuu", "uuu", 1));


# Test coding region is what we expect
sub testGetCodingRegions(\%) {
  my %dna = %{$_[0]};
  note(Dumper(\%dna));
  return Genes::GetCodingRegions(%dna);
}

sub testHighlightCodingRegions(\%) {
  my %dna = %{$_[0]};
  return Genes::HighlightCodingRegions(%dna);
}

my %dna1;
$dna1{sequence} = "aaaaaacccaaa";
$dna1{cds} = "7..9"; #test for single coding region

is (testGetCodingRegions(%dna1), "ccc", "Coding region should be ccc");
is (testHighlightCodingRegions(%dna1), "aaaaaaCCCaaa", "Sequence should be aaaaaaCCCaaa");

my %dna2;
$dna2{sequence} = "aaaaaabbbaaabbb";
$dna2{cds} = "7..9,13..15"; # test multiple coding regions

is (testGetCodingRegions(%dna2), "bbbbbb", "Coding region should be bbbbbb");
is (testHighlightCodingRegions(%dna2), "aaaaaaBBBaaaBBB", "Sequence should be aaaaaaBBBaaaBBB");

done_testing();

