#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use File::Spec;
use lib File::Spec->catdir($FindBin::Bin, '..', '../data');

use Test::More;
use Display;
use Data::Dumper;


my $extractedDna = 'aaAGaaT';  
my $presetEnzyme='';
my $userEnzymeSearch='aaaa';

sub test

is (Display::ShowCodingAndEnzymeRegion($extractedDna,$presetEnzyme,$userEnzymeSearch)
Display::ShowCodingAndEnzymeRegion($extractedDna,$presetEnzyme,$userEnzymeSearch)


# Test that list genes returns mock data
sub testListGenes() {
  my %genes = Genes::ListGenes();
  note(Dumper(\%genes));
  my $count = scalar (keys %genes);
  return $count;
}

is ( testListGenes(), 7, "ListGenes with no params should contain 7 rows");

sub testListGenesWithFilter($$) {
  my $searchword = $_[0];
  
  my $filter = $_[1];
  my %genes = Genes::ListGenes($searchword,$filter);
  return scalar (keys %genes);
}

is ( testListGenesWithFilter("GI:111111", "geneid"), 1, "Search for 'geneid=GI:111111' should return 1 result");
is ( testListGenesWithFilter("", "geneid"), 7, "Search for 'geneid=' should return 7 results");
is ( testListGenesWithFilter("protein product", "product"), 6, "Search for 'product=protein product' should return 6 results");
is ( testListGenesWithFilter("AB000001", "accession"), 1, "Search for 'accession=AB000001' should return 1 result");
is ( testListGenesWithFilter("3q4", "location"), 2, "Search for 'location=3q4' should return 2 result");
is ( testListGenesWithFilter("AB000005", "accession"), 1, "Search for 'accession=AB000005' should return 1 result");


# test getgene returns the correct gene details
my %test= Genes::GetGene("GI:111111");
my $expectedaccession = "AB000001";
my $actualaccession = $test{accession};
is ($actualaccession,$expectedaccession,"Your expected and actual accession match");


# test that codon count is what we expect
sub testCodonFrequencies($$$) {
  my $dna = $_[0];
  my $codonToTest = $_[1];
  my $expectedCount = $_[2];
  my %freq = Genes::GetCodonFrequencies($dna);
  note(Dumper(\%freq));
  if (exists($freq{uc($codonToTest)}) && $freq{uc($codonToTest)} == $expectedCount) {
    return 1;
  }
  return 0;
}

ok (testCodonFrequencies("uuuuuu", "uuu", 2));
ok (testCodonFrequencies("uuucccuu", "uuu", 1));


sub testGetCodingRegions(\%) {
  my %dna = %{$_[0]};
  note(Dumper(\%dna));
  return Genes::GetCodingRegions(%dna);
}

my %dna1;
$dna1{sequence} = "aaaaaacccaaa";
$dna1{cds} = "7..9";

is (testGetCodingRegions(%dna1), "ccc", "Coding region should be ccc");

my %dna2;
$dna2{sequence} = "aaaaaabbbaaabbb";
$dna2{cds} = "7..9,13..15";

is (testGetCodingRegions(%dna2), "bbbbbb", "Coding region should be bbbbbb");

done_testing();

