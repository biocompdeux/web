package Genes;
use strict;
use Exporter;

use lib qw(../data/);
use GeneData;

sub ListGenes;
sub GetGene;
sub loadGenes;
sub getDNA;
sub getAA;
sub GetCodonFrequencies;
sub GetCodingRegions;
sub GetChromosomeCodonFrequencies;

# ListGenes =========================================================
# returns a hash keyed by geneId of a summary of gene information
# < $search (optional) - str, search value
# < $filter (optional) - str, filter by: accession, location, product
# > %genes - hash of results
#   %genes{$geneid} =>{ 'location': str, 'product': str, 'accession': str };
sub ListGenes {
  my $search = $_[0]; # search term eg. GI:3077768
  my $filter = $_[1];	# filter by one of: [geneid|product|location|accession]
  my %genes = GeneData::GetGenes(); # get all genes from data layer
  my %filteredGenes; # new hash containing only genes user has searched for

  if (defined $search && length(trim($search))) { # user has provided a search term
    # find all matches in %genes
    my $count = 0; # number of matches

########
# filter by geneid
# user can provide complete geneid or just the numbers without GI: to search. 
    if($filter eq "geneid"){
       
       foreach my $geneid (%genes){
         
          if ($geneid =~ /(.*)$search/i){
            $filteredGenes{$geneid}{product} = $genes{$geneid}{product};
            $filteredGenes{$geneid}{location} = $genes{$geneid}{location};
            $filteredGenes{$geneid}{accession} = $genes{$geneid}{accession};
            $count++;
           
          } 
       }
    }
    
# filter by product name  
# partial search term is allowed fot product name  
    if($filter eq "product"){
       
       foreach my $geneid (%genes){
        
        my $product = $genes{$geneid}{product};
        
          if ($product =~ /$search/i){
                   
            $filteredGenes{$geneid}{product} = $genes{$geneid}{product};
            $filteredGenes{$geneid}{location} = $genes{$geneid}{location};
            $filteredGenes{$geneid}{accession} = $genes{$geneid}{accession};
            $count++;
           
           
          } 
       }
    }
    
# filter by accession
# user can provide partial accession to search
    if($filter eq "accession"){
       
       foreach my $geneid (%genes){
          my $accession = $genes{$geneid}{accession};
         
          if ( $accession =~ /$search/i){
            $filteredGenes{$geneid}{product} = $genes{$geneid}{product};
            $filteredGenes{$geneid}{location} = $genes{$geneid}{location};
            $filteredGenes{$geneid}{accession} = $genes{$geneid}{accession};
            $count++;
           
          } 
       }
    }
    
# filter by location   
# partial location is allowed as location search. This is to give flexibility to the user  
    if($filter eq "location"){
       
       foreach my $geneid (%genes){
          my $location = $genes{$geneid}{location};
          
          if ( $location =~ /$search/i){
            $filteredGenes{$geneid}{product} = $genes{$geneid}{product};
            $filteredGenes{$geneid}{location} = $genes{$geneid}{location};
            $filteredGenes{$geneid}{accession} = $genes{$geneid}{accession};
            $count++;
           
          } 
       }
    }
    
  } else { # no search string provided no filtering needed
    %filteredGenes = %genes;
  }
  return %filteredGenes;
}

# GetGene ===========================================================
# return a hash containing a gene
# < $geneid (required) - str, GI number
# > %gene - hash of a gene containing summary info (product, location,
#           accession, in addition to DNA, AA sequence, etc)
#   %gene => { 'id': str, 'product': str, 'location', 'accession' : str, 'dna': str, 'exons': str, 'geneCodonFreq' : hash }

sub GetGene($) {
  my $geneid = $_[0];        ## passed from getgene.pl by Sand
  my %gene = GeneData::GetGene($geneid);
  my %dna = GeneData::GetDNA($geneid);
  $gene{dna} = HighlightCodingRegions(\%dna);
  my $dnaCodingSeq = GetCodingRegions(\%dna);
  $gene{exons} = $dnaCodingSeq;
  $gene{geneCodonFreq} = GetCodonFrequencies($dnaCodingSeq);
  return %gene;
}

# GetCodonFrequencies ===============================================
# returns coding frequencies of given DNA sequence
# < $dna
# > %codingFrequencies: keyed by 3-letter codons
sub GetCodonFrequencies($) {
  my $str = $_[0];
  my @codons = $str =~ /\w{3}/g;
  my %freq;
  foreach my $codon (@codons) {
    $freq{uc($codon)}++;
  }
  return %freq;
}


# GetChromosomeCodonFrequencies =====================================
# returns codon frequencies of the entire chromosome
# > %codingFrequencies: keyed by 3-letter codons
# TODO: not ideal way of retrieving dna sequence
sub GetChromosomeCodonFrequencies() {
  my %genes = GeneData::GetGenes();
  my $chromosomeCodingSequence;
  foreach my $geneid (%genes) {
    my %dna = GeneData::GetDNA($geneid);
    $chromosomeCodingSequence .= GetCodingRegions(\%dna);
  }
  return GetCodonFrequencies($chromosomeCodingSequence);
}


# GetCodingRegions ==================================================
# returns only the coding regions given a DNA sequence
# < ($sequence, @exons) - full sequence and array of coding region locations
# > $dnaSeq - coding regions only
sub GetCodingRegions(\%) {
  my %dna = %{$_[0]};
  my $str = $dna{sequence};
  my $cds = $dna{cds};
  my $codingSeq;
  foreach my $region (split(',', $cds)) {
    if ($region =~ /([\d]+)\.\.([\d]+)/) {
      my ($exonStart, $exonEnd) = ($1, $2);
      my $start = $exonStart - 1;
      my $length = $exonEnd - $exonStart + 1;
      $codingSeq .= substr($str, $start, $length);
    }
  }
  return $codingSeq;
}


# HighlightCodingRegions ============================================
# returns the entire DNA sequence with exons in uppercase
# < ($sequence, @exons) - full sequence and array of coding region locations
# > $str - full sequence, with exons in uppercase
sub HighlightCodingRegions(\%) {
  my %dna = %{$_[0]};
  my $str = $dna{sequence};
  my $cds = $dna{cds};
  my $complement = $dna{complement};
  foreach my $region (split(',', $cds)) {
    if ($region =~ /([\d]+)\.\.([\d]+)/) {
      my ($exonStart, $exonEnd) = ($1, $2);
      my $start = $exonStart - 1;
      my $length = $exonEnd - $exonStart + 1;
      my $tailStart = $exonEnd;
      my $tailLength = length($str) - $tailStart;
      $str = substr($str, 0, $start).uc(substr($str, $start, $length)).substr($str, $tailStart, $tailLength);
    }
  }

  # If the Coding Region is intended to be read backwards
  if ($complement) {
    return reverse $str;
  }
  return $str;
}

# useful subroutines

# trim
# remove whitespace from both ends of the string
sub trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

1;
