#!/usr/bin/perl
# getgene.pl ## This script displays detailed information on a gene given the geneid

use warnings;
use CGI;
use strict;
use Data::Dumper;
use CGI::Carp qw/fatalsToBrowser/;
use lib qw(app web data);
use Display;

my ($gi, %gene, $presetEnzyme, $userEnzymeSearch, $codonFrequencies, @dna);
my $cgi = new CGI;
$gi = $cgi->param( 'gi' );
%gene = Display::GetGene($gi);
my $extractedGi=$gene{geneid};
my $extractedAcc=$gene{accession};
my $extractedLoc=$gene{location};
my $extractedProd=$gene{product};
my $extractedDna=$gene{dna};
my $extractedAa=$gene{aa};
my $codingDna= $gene{exons};
$presetEnzyme = $cgi->param('enzyme');
$userEnzymeSearch = $cgi->param('enzymesearch');
$codonFrequencies = Display::ShowCodonFrequencies($codingDna);


##### restriction enzyme search and associated messages ############ 
@dna=Display::ShowCodingAndEnzymeRegion($extractedDna, $presetEnzyme, $userEnzymeSearch);
my $taggedDna=$dna[0];
my $hitCount=$dna[1];
my $searchMessage=$dna[2];
my $resultsMessage=$dna[3];

##### places GI number in header bar ###############################
my $title = "Details of gene $extractedGi&nbsp;&nbsp;"; 

## establish environment for different users within our group ######
my $userName = $ENV{LOGNAME} || $ENV{USER} || getpwuid($<);

#### HTML5 code using Bootstrap ####################################
print $cgi->header(-charset=>'utf-8');

print <<__EOF;
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>gene details</title>

  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <!-- Custom styles for this template -->
  <link href="/~$userName/css/custom.css" rel="stylesheet">
 
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div id="navbar" class="navbar-collapse collapse">
	<div class="navbar-header">
	  <a class="navbar-brand" href="listgenes.pl">Chromosome 3</a>
	</div><!-- class="navbar-header" -->
	<div class="navbar-form navbar-right">
	  <a class="title">$title</a>
	  <a class="btn btn-info" href="#dna" role="button">DNA</a>
	  <a class="btn btn-info" href="#aa" role="button">AA</a>
    	  <a class="btn btn-info" href="#codon" role="button">Codon</a>
	</div><!-- class="navbar navbar-right" -->
      </div><!-- id="navbar" class="navbar-collapse collapse" -->
    </div><!-- class="container" -->
 </nav><!-- class="navbar navbar-inverse" -->

<!-- Begin page content -->

<!-- this is where the coding and non coding DNA results go -->
<!-- this will allow users to search for restiction enzyme sites -->

  <div class="container">
    <div class="placeholder" id ="dna"><!-- places details in centre of screen -->
      <div class ="resultsDNA">
        <h2>DNA</h2>
	  <p class="descriptor">The DNA sequence for gene $extractedGi is shown below.  Exons are highlighted yellow.
	    Search for restriction enzyme binding sites using the drop down menu, or enter your own enzyme sequence.  
	    Enzymes that <strong>only</strong> bind at sites before and after entire coding region are considered useful.</p>
            <p>$searchMessage  $resultsMessage</p>
	<div class="navbar navbar-right">
	  <form class="navbar-form navbar-right form-inline" action="getgene.pl#dna">
            <div class = "form-group">
              <div class = "enzchoice">
                <select class="form-control" name = "enzyme" size = 1>
                  <option value="">Select Enzyme</option>
                  <option value="ggatcc">BamHI</option>
                  <option value="agatct">Bg1 II</option>
                  <option value="ctcgag">BsuMI</option>
                  <option value="atcgat">C1a1</option>
                  <option value="tttaaa">Dra1</option>
                  <option value="gaattc">EcorI</option>
                  <option value="gatatc">EcoRV</option>
                  <option value="aagctt">HindIII</option>
                  <option value="gcggccgc">NotI</option>
                  <option value="ctgcag">Pst1</option>
                  <option value="gtcgac">Sal I</option>
                  <option value="cccggg">SmaI</option>
                  <option value="ccwwgg">StyI</option>
                  <option value="cccggg">XmaI</option>
                </select>
		<input type="hidden" name="gi" value="$gi"/>
		<input type="text" name="enzymesearch" placeholder="or your own enzyme" class="form-control">
		<button type="submit" class="btn btn-info">Search</button>
             </div><!-- class="enzchoice" -->
            </div><!-- class="form-group" -->
	  </form>
        </div><!-- class="navbar-right" -->
        <div><!-- separated results from buttons -->
	 <div class = "results">
	   <p class="text-lowercase">$taggedDna</p>
	 </div><!-- class="results" -->
        </div><!-- separated results from buttons -->
     </div><!-- class="resultsDNA" -->
    </div><!-- class="placeholder" -->
  </div><!-- class="container" -->

<!-- this is where the amino acid sequence and correspondng DNA results go -->

  <div class="container">
    <div class="placeholder" id ="aa"><!-- places details in centre of screen -->
      <div class ="resultsAA">
	<h2>AA</h2>
	  <p class="descriptor">The AA sequence for gene $extractedGi is shown below.  The relevant DNA coding region is also shown.</p>
        <div class="results">
	  <p class="text-uppercase"><span class="seqtitle">AA sequence</span> $extractedAa</p>
	  <p class="text-lowercase"><span class="seqtitle">DNA coding sequence</span> $codingDna</p>
	</div><!-- class="results" -->
      </div><!--class="resultsAA" -->
    </div><!-- class="placeholder" -->
  </div><!--"container" -->  

<!-- bottom of page details, anchored to bottom -->

  <footer class="footer">
    <nav class = "navbar navbar-default navbar-fixed-bottom">
      <div class="container">
	<p class="text-muted">MSc Bioinformatics, Birkbeck &mdash; &copy; Biocomputing II Team III, 2016</p>
      </div><!-- class="container" -->
    </nav><!-- class = "navbar navbar-default navbar-fixed-bottom" -->
  </footer><!-- class = "footer" -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- highcharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- this is where the codon frequency table goes -->
 
  <div class="container">
    <div class="placeholder" id ="codon"><!-- places details in centre of screen -->
      <div class="resultsCodon">
        <h2>Codon Frequency Table</h2>
         <p class="descriptor">The percentage frequency of codons in gene $extractedGi are compared with the frequency of codons in Chromosome 3</p>
	 <p>$codonFrequencies</p>
	<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </div><!-- "resultsCodon" -->
    </div><!-- class="placeholder" -->
  </div><!-- class="container" -->  

</body>
</html>
__EOF

