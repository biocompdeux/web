#!/usr/bin/perl -w

#This perl test script illustrates how the data extraction and load tool temporary stores all
#relevant gene information in a hashmap before the data being inserted into a relational database schema

#Simply run this file. No command line arguments necessary.

#It uses real data from gene identifier GI:4835382 within the chromosome 3 genbank file

use warnings;
use strict;

my %data;

my %gene;
my %codingRegion;
my %codingRegionExon;
my $geneCount = 0;
my $codingRegionCount = 0;
my $codingRegionExonCount = 0;

#Each Gene Identifier in the GenBank file is separated by a special token: //

#Note: //VERSION  VERSION     AB026898.1  GI:4835382
$gene{'geneIdentifier'} = 'GI:4835382';

#Note: FEATURES, source, /map="xxx"
$gene{'mapLocation'} = '3p22-p21.3';

#Note: ACCESSION   AB026898 AB010443
$gene{'accession'} = 'AB026898 AB010443';

#Note: ORIGIN  gttagcggcg (etc.. 269941 characters?)
$gene{'nucleotideSequence'} = 'gttagcggcg';

#Coding regions are loaded incrementally below.
#%{$codingRegion{'codingRegion'}} = %codingRegion;

#Note: Immediately following CDS, the first of 'product' or 'standard_name'
#Needs error handling here. Not standard names
$codingRegion{'product'} = 'deleted in lung and esophageal cancer 1';
$codingRegion{'aminoAcidSequence'} = 'METRSSKTRRSLASRTNECQGTMWAPTSPPAGSSSPSQPTWKSSLYSSLAYSEAFHYSFAARPRRLTQLALAQRPEPQLLRLRPSSLRTQDISHLLTGVFRNLYSAEVIGDEVSASLIKARGSENERHEEFVDQLQQIRELYKQRLDEFEMLERHITQAQARAIAENERVMSQAGVQDLESLVRLPPVKSVSRWCIDSELLRKHHLISPEDYYTDTVPFHSAPKGISLPGCSKLTFSCEKRSVQKKELNKKLEDSCRKKLAEFEDELDHTVDSLTWNLTPKAKERTREPLKKASQPRNKNWMNHLRVPQRELDRLLLARMESRNHFLKNPRFFPPNTRYGGKSLVFPPKKPAPIGEFQSTEPEQSCADTPVFLAKPPIGFFTDYEIGPVYEMVIALQNTTTTSRYLRVLPPSTPYFALGLGMFPGKGGMVAPGMTCQYIVQFFPDCLGDFDDFILVETQSAHTLLIPLQARRPPPVLTLSPVLDCGYCLIGGVKMTRFICKNVGFSVGRFCIMPKTSWPPLSFKAIATVGFVEQPPFGILPSVFELAPGHAILVEVLFSPKSLGKAEQTFIIMCDNCQIKELVTIGIGQLIALDLIYISGEKSQPDPGELTDLTAQHFIRFEPENLRSTARKQLIIRNATHVELAFYWQIMKPNLQPLMPGETFSMDSIKCYPDKETAFSIMPRKGVLSPHTDHEFILSFSPHELRDFHSVLQMVLEEVPEPVSSEAESLGHSSYSVDDVIVLEIEVKGSVEPFQVLLEPYALIIPGENYIGINVKKAFKMWNNSKSPIRYLWGKISDCHIIEVEPGTGVIEPSEVGDFELNFTGGVPGPTSQDLLCEIEDSPSPVVLHIEAVFKGPALIINVSALQFGLLRLGQKATNSIQIRNVSQLPATWRMKESPVSLQERPEDVSPFDIEPSSGQLHSLGECRVDITLEALHCQHLETVLELEVENGAWSYLPVYAEVQKPHVYLQSSQVEVRNLYLGVPTKTTITLINGTLLPTQFHWGKLLGHQAEFCMVTVSPKHGLLGPSEECQLKLELTAHTQEELTHLALPCHVSGMKKPLVLGISGKPQGLQVAITISKESSDCSTEQWPGHPKELRLDFGSAVPLRTRVTRQLILTNRSPIRTRFSLKFEYFGSPQNSLSKKTSLPNMPPALLKTVRMQEHLAKREQLDFMESMLSHGKGAAFFPHFSQGMLGPYQQLCIDITGCANMWGEYWDNLICTVGDLLPEVIPVHMAAVGCPISSLRTTSYTIDQAQKEPAMRFGTQVSGGDTVTRTLRLNNSSPCDIRLDWETYVPEDKEDRLVELLVFYGPPFPLRDQAGNELVCPDTPEGGCLLWSPGPSSSSEFSHETDSSVEGSSSASNRVAQKLISVILQAHEGVPSGHLYCISPKQVVVPAGGSSTIYISFTPMVLSPEILHKVECTGYALGFMSLDSKVEREIPRKRHRLQDFAVGPLKLDLHSYVRPAQLSVELDYGGSMEFQCQASDLIPEQPCSGVLSELVTTHHLKLTNTTEIPHYFRLMVSRPFSVSQDGASQDHRAPGPGQKQECEEETASADKQLVLQAQENMLVNVSFSLSLELLSYQKLPADQTLPGVDIQQSASGEREMVFTQNLLLEYTNQTTQVVPLRAVVAVPELQLSTSWVDFGTCFVSQQRVREVYLMNLSGCRSYWTMLMGQQEPAKAAVAFRVSPNSGLLEARSANAPPTSIALQVFFTARSSELYESTMVVEGVLGEKSCTLRLRGQGSYDERYMLPHQP';


$codingRegion{'complement'} = 0;

#Note:      join(22..432,6340..6490,20494..20604,22914..23113,
#                     23326..23546,24583..24661,32557..32644,32963..33136,
#                     33929..34065,35984..36076,40485..40575,41301..41463,
#                     42579..42771,43593..43651,44275..44443,44888..44981,
#                     45213..45343,45450..45608,47998..48137,55303..55456,
#                     56126..56236,57153..57283,57820..58002,59401..59471,
#                     59940..60091,62072..62190,62714..62784,64185..64390,
#                     64505..64621,64707..64832,64940..65034,65259..65343,
#                     65538..65756,68181..68342,69361..69514,69804..69927,
#                     70022..70145)

#37, but just use the first three
$codingRegion{'numberOfExons'} = 3;

$codingRegionExonCount = 0;
$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 22;
$codingRegionExon{'stopBase'} = 432;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
$codingRegionExonCount ++;

$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 6340;
$codingRegionExon{'stopBase'} = 6490;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
$codingRegionExonCount ++;

$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 20494;
$codingRegionExon{'stopBase'} = 20604;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;

# Store number of exons in CDS for looping in order
%{$gene{'codingRegion' . $codingRegionCount}} = %codingRegion;
$codingRegionCount ++;

$codingRegion{'product'} = 'organic-cation transporter like 3';
$codingRegion{'aminoAcidSequence'} = 'MAQFVQVLAEIGDFGRFQIQLLILLCVLNFLSPFYFFAHVFMVLDEPHHCAVAWVKNHTFNLSAAEQLVLSVPLDTAGHPEPCLMFRPPPANASLQDILSHRFNETQPCDMGWEYPENRLPSLKNEFNLVCDRKHLKDTTQSVFMGGLLVGTLMFGPLCDRIGRKATILAQLLLFTLIGLATAFVPSFELYMALRFAVATAVAGLSFSNVTLLTEWVGPSWRTQAVVLAQCNFSLGQMVLAGLAYGFRNWRLLQITGTAPGLLLFFYFWALPESARWLLTRGRMDEAIQLIQKAASVNRRKLSPELMNQLVPEKTGPSGNALDLFRHPQLRKVTLIIFCVWFVDSLGYYGLSLQVGDFGLDVYLTQLIFGAVEVPARCSSIFMMQRFGRKWSQLGTLVLGGLMCIIIIFIPADLPVVVTMLAVVGKMATAAAFTISYVYSAELFPTILRQTGMGLVGIFSRIGGILTPLVILLGEYHAALPMLIYGSLPIVAGLLCTLLPETHGQGLKDTLQDLELGPHPRSPKSVPSEKETEAKGRTSSPGVAFVSSTYF';

$codingRegion{'complement'} = 0;

#Note: join(214850..215227,223261..223364,223593..223747,
#			 223978..224146,224359..224479,224567..224661,
#            224871..225085,225276..225384,225901..226116,
#             226361..226454)

#9, but just use the first three
$codingRegion{'numberOfExons'} = 3;

$codingRegionExonCount = 0;
$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 214850;
$codingRegionExon{'stopBase'} = 215227;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
$codingRegionExonCount ++;

$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 223261;
$codingRegionExon{'stopBase'} = 223364;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
$codingRegionExonCount ++;

$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 223593;
$codingRegionExon{'stopBase'} = 223747;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;

# Store number of exons in CDS for looping in order
%{$gene{'codingRegion' . $codingRegionCount}} = %codingRegion;
$codingRegionCount ++;


$codingRegion{'product'} = 'organic-cation transporter like 4';
$codingRegion{'aminoAcidSequence'} = 'MAGEENFKEELRSQDASRNLNQHEVAGHPHSWSLEMLLRRLRAVHTKQDDKFANLLDAVGEFGTFQQRLVALTFIPSIMSAFFMFADHFVFTAQKPYCNTSWILAVGPHLSKAEQLNLTIPQAPNGSFLTCFMYLPVPWNLDSTIQFGLNDTDTCQDGWIYPDAKKRSLINEFDLVCGMETKKDTAQIMFMAGLPIGSLIFRLITDKMGRYPAILLSLLGLIIFGFGTAFMNSFHLYLFFRFGISQSVVGYAISSISLATEWLVGEHRAHAIILGHCFFAVWAMLLTGIAYGLPHWQLLFLVGGILVIPFISYIWILPESPRWLMMKGKVKEAKQVLCYAASVNKKTIPSNLLDELQLPRKKVTRASVLDFCKNRQLCKVTLVMSCVWFTVSYTYFTLSLRMRELGVSVHFRHVVPSIMEVPARLCCIFLLQQIGRKWSLAVTLLQAIIWCLLLLFLPEGEDGLRLKWPRCPATELKSMTILVLMLREFSLAATVTVFFLYTAELLPTVLRATGLGLVSLASVAGAILSLTIISQTPSLLPIFLCCVLAIVAFSLSSLLPETRDQPLSESLNHSSQIRNKVKDMKTKETSSDDV';
$codingRegion{'complement'} = 0;

#     CDS             join(255030..255545,256256..256359,256569..256723,
#                     257873..258041,261918..262038,262307..262404,
#                     262666..262880,264498..264651,265264..265464,
#                     267137..267188)
#13, but just use first three
$codingRegion{'numberOfExons'} = 3;

$codingRegionExonCount = 0;
$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 255030;
$codingRegionExon{'stopBase'} = 255545;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
$codingRegionExonCount ++;

$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 256256;
$codingRegionExon{'stopBase'} = 256359;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
$codingRegionExonCount ++;

$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
$codingRegionExon{'startBase'} = 256569;
$codingRegionExon{'stopBase'} = 256723;
%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;

%{$gene{'codingRegion' . $codingRegionCount}} = %codingRegion;
$codingRegionCount ++;
$gene{'numberOfCodingRegions'} = $codingRegionCount;

%{$data{'gene' . $geneCount}} = %gene;
$geneCount++;

#data now holds everything
#wipe the other three temp hashes
%gene = ();
%codingRegion = ();
%codingRegionExon = ();

print "\n\nStart\n\n";

for (my $i = 0; $i < $geneCount; $i++) {
	%gene = %{$data{'gene' . $i}};
	
	print 'geneIdentifier: ' . $gene{'geneIdentifier'} . "\n";
	print 'mapLocation: ' . $gene{'mapLocation'} . "\n";
	print 'accession: ' . $gene{'accession'} . "\n";
	print 'nucleotideSequence: ' . $gene{'nucleotideSequence'} . "\n";
	
	print 'products' . "\n";
	my $numberOfCodingRegions = $gene{'numberOfCodingRegions'};
	for (my $j = 0; $j < $numberOfCodingRegions; $j++) {

		%codingRegion = %{$gene{'codingRegion' . $j}};

		print '  | product: ' . $codingRegion{'product'} . "\n";
		print '  | aminoAcidSequence: ' . $codingRegion{'aminoAcidSequence'} . "\n";
		print '  | complement: ' . $codingRegion{'complement'} . "\n";
		print '  | exons' . "\n";

		my $numberOfExons = $codingRegion{'numberOfExons'};
		for (my $k = 0; $k < $numberOfExons; $k++) {
			my %codingRegionExon = %{$codingRegion{'codingRegionExon' . $k}};
			print '      | sortOrder: ' . $codingRegionExon{'sortOrder'} . "\n";
			print '      | startBase: ' . $codingRegionExon{'startBase'} . "\n";
			print '      | stopBase: ' . $codingRegionExon{'stopBase'} . "\n";
		}
	}
}

print "\n\nFinish\n\n";

#foreach my $speciesName (keys %viralSpeciesLookup) {

#}



=comment

$gene{'geneIdentifier'} = $geneIdentifier;
$gene{'mapLocation'} = $mapLocation;
$gene{'accession'} = $accession;
$gene{'nucleotideSequence' = $nucleotideSequence;
%{$codingRegion{'codingRegion'}} = %codingRegion;

my %hashTable;
#If the value contains a comma, treat it as a list (array)
if ($value =~ /,/g) {
	my @values = split(/, /, $value);
	@{$hashTable{$key}} = @values;
} else {
	$hashTable{$key} = $value;
}   
return ('', %hashTable);

foreach my $speciesName (keys %viralSpeciesLookup) {

}

gene is a hashmap

if ($value =~ /,/g) {
	my @values = split(/, /, $value);
	@{$hashTable{$key}} = @values;
} else {
	$hashTable{$key} = $value;
}   
=cut