package GeneData;

use strict;
use warnings FATAL => 'all';
use Exporter;
use LWP::Simple;
use datalayer;
use Cwd;

sub loadGenes;
sub GetGenes;
sub GetDNA;

# loadGenes -----------------------------------------------------------------------------
# read genes from data source (currently static text file)
# > @genes -- list of gene info in the format: geneid|product|location|accession
#sub loadGenes() {
  # read in known genes
#  my $username = $ENV{LOGNAME} || $ENV{USER} || getpwuid($<);
#  my $url = 'http://student.cryst.bbk.ac.uk/~'.$username.'/data/chr3genes.txt';
#  my $content  = get $url;
#  die "Couldn't get $url" unless defined $content;
#  my @genes = split(/\n/, $content);

#  return @genes;
#}

# parseGenes -----------------------------------------------------------------------------
# parses genes info from format: geneid|product|location|accession
# > @genes -- list of gene info in the format:
#sub parseGenes(@) {
#  my @genelist = @{$_[0]};
#  my %genes;
#  foreach my $geneLine (@genelist) {
#    my ($geneid, $product, $location, $accession, $aa) = split(/\|/, $geneLine);
#    if ($geneid =~ /GI:/) {
#      $genes{$geneid}{product} = $product;
#      $genes{$geneid}{location} = $location;
#      $genes{$geneid}{accession} = $accession;
#      $genes{$geneid}{aa} = $aa;
#    }
#  }
#  return %genes;
#}

# GetGenes ==============================================================================
# returns genes from data source in a hash
# > %genes:
#   $genes{$geneid}{product|location|accession}
sub GetGenes() {

    my $errors = datalayer::readConfig('data/config.ini');
    if (length($errors) > 0) {
        return "Current working directory is" . getcwd() . "\n" . $errors;
        return "Errors occured during GetDNA: " . $errors;
        warn "Errors occured during GetDNA: " . $errors;
    }

    return datalayer::getGenes();

    #The remainder of this subroutine is ignored
    #

#  my @genelist = loadGenes();
#  my %genes = parseGenes(\@genelist);
#  return %genes;
}

# GetGene ===============================================================================
# return a hash containing a gene
# < $geneid (required) - str, GI number
# > %gene - hash of a gene containing summary info (product, location,
#           accession, in addition to DNA, AA sequence, etc)
#   %gene => { 'id': str, 'product': str, 'location', 'accession' : str }
sub GetGene($) {
    my $geneIdentifier = $_[0];
 
    my $errors = datalayer::readConfig('data/config.ini');
    if (length($errors) > 0) {
        warn "Errors occured during GetDNA: " . $errors;
    }

    return datalayer::getGene($geneIdentifier);

    #The remainder of this subroutine is ignored
    #
#  my $geneid = $_[0];
#  my %genes = GetGenes(); # TODO: return one gene only
#  my %gene;
#  $gene{geneid} = $geneid;
#  $gene{product} = $genes{$geneid}{product};
#  $gene{location} = $genes{$geneid}{location};
#  $gene{accession} = $genes{$geneid}{accession};
#  $gene{aa} = $genes{$geneid}{aa};
#  return %gene;
}

# GetDNA ================================================================================
# return DNA sequence given geneid
# < $geneid
# > %dnaSeq - hash containing dna sequence and array of coding region positions
#   $dnaSeq{sequence} - the full sequence (exons and introns)
#   $dnaSeq{exons} - exon location
#   %dnaSeq => { 'sequence': str, 'cds': str }
sub GetDNA($) {

    my $geneIdentifier = $_[0];

    my $errors = datalayer::readConfig('data/config.ini');
    if (length($errors) > 0) {
        warn "Errors occured during GetDNA: " . $errors;
    }

    return datalayer::getDNA($geneIdentifier);
  
    #The remainder of this subroutine is ignored


 # my $geneid = $_[0];
  # TODO get DNA sequence from DB
#  my $dnaString = '
#        1 ttgctcccaa gagggaacgt aagacggtga gtagcagtga ggggctccgg gacatctggg
#       61 aaggggagaa tcaaggtcag gtgctgctgg gacattgtgc cggaaagagc agtgacttga
#      121 ggaggaagga gctgagagtg ggcagagaat ggtgtctgtg gagggctcag gttctttgta
#      181 agatgctaag atttcagcat agaagcaggt catgggaaga ctgaagtgcc tagcgaggaa
#      241 gttcctgcga gacctgggtg ctggggggtt ggaagaacta tgcttggttt tgggtttcta
#      301 gtggcttgcc ttggaaatgg tttgttttct gtggccccag cccccattcc cccctgcccc
#      361 cccagtcctg gaagctctca aggcctccgc ctctgctcag tcctcaagcc ctgggcgtgg
#      421 tgcccagaat agggtgccag agctggggaa gccgctgagt caccctggct ccacctactg
#      481 gatctgggcc ctgcccccag agatcaggca cactagccac aagtgagccg attgggttct
#      541 agatgggggt cctgggcccc agggtgtgca gccactgact tggggactgc tggtggggta
#      601 gggatgaggg agggaggggc attgtgatgt acagggctgc tctgggagat caagggtctc
#      661 ttaagggtgg gagctggggc agggactacg agagcagcca gatgggctga aagtggatct
#      721 caaggggttt ctggcaccta cctacctgct tcccgctggg gggtggggag ttggcccaga
#      781 gtcttaagat tggggcaggg tggagaggtg ggctcttcct gcttcccact catcttatag
#      841 ctttctttcc ccagatccga attcgagatc caaaccaagg aggaaaggat atcacggagg
#      901 agatcatgtc tggggcccgc actgcctcca cacccacccc tccccaggta ctgtctgctt
#      961 ttcgagtgat accctggctg ggatgtctgt tctgccctgg actcccaagt ccctggatct
#     1021 tttctttcaa ctaagggatt tatttccctg ctttcctgga tttctaggca gagctaaagt
#     1081 agaaatggct ctagtaaaga agggttgtgg gactcttcag tgcaaacttg gtaacccttt
#     1141 gtgtcctgca gacgggaggc ggtctggagc ctcaagctaa tggggagacg ccccaggttg
#     1201 ctgtcattgt ccggccagat gacc';
#  $dnaString =~ s/\s+//g;
#  $dnaString =~ s/[0-9]+//g;
#  my $codingRegions = "21..57,58..100,101..200,281..500,501..700";
#  my %dnaSeq;
#  $dnaSeq{sequence} = $dnaString;
#  $dnaSeq{cds} = $codingRegions;
#  return %dnaSeq;
}

1;
