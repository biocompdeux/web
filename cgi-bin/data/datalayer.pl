#!/usr/bin/perl -w

#This script will take a single argument 
#Either a zipped or unzipped chromosomefile
#If the supplied file is zipped, an unzipped version will be created and parsed
#Usage:
#         ./datalayer.pl <ChromosomeFile>
#
#This script:
#	- performs various validation checks
#	- reads in the configuration file
#	- extracts all meaningful information from the chromosome file
#	- stores all data in a correct heirachy
#	- parses the data into a relational database schema
#	- does a few simple post processing tests
#
#The simple tests at the end of this file also pose as examples for those wishing to
#use the package datalayer.pm

use warnings;
use strict;
use datalayer;

#Define script variables
my ($errors, $chromosomeFile, %chromosomeData);

#Read configuration information
print "\nReading configuration file\n";
$errors = datalayer::readConfig('config.ini');
#If errors exist, stop processing
if (length($errors) > 0) { die($errors); }
print "Success reading configuration file\n\n";

#Validate Database connection
print "Checking database connection\n";
$errors = datalayer::checkDatabaseConnection();
#If errors exist, stop processing
if (length($errors) > 0) { die($errors); }
print "Success checking database connection\n\n";

#Validate command line
print "Validating command line\n";
($errors, $chromosomeFile) = datalayer::checkCommandLineAndFiles();
#If errors exist, stop processing
if (length($errors) > 0) { die($errors); }
print "Success validating command line\n\n";

#Read in the input file
print "Parsing the chromosome input file: $chromosomeFile\n";
($errors, %chromosomeData) = datalayer::extractDataFromFile($chromosomeFile);
#If errors exist, stop processing
if (length($errors) > 0) { die($errors); }
print "Success parsing the chromosome input file\n\n";


#For debugging purposes: print the data (not including nucleotides)
#datalayer::printChromosomeData(\%chromosomeData);


#Load the data into the database
print "Inserting the chromosome data into the database\n";
$errors = datalayer::loadDataIntoDatabase(\%chromosomeData);
#If errors exist, stop processing
if (length($errors) > 0) { die($errors); }
print "Success inserting the chromosome data into the database\n\n";

print "A few quick tests\n\n";
print "getDNA()\n";
my $geneIdentifier = 'GI:16943789';
my %dnaSeq = datalayer::getDNA($geneIdentifier);
print "Gene                : $geneIdentifier\n";
print "Sequence (40 chars) : " . substr($dnaSeq{sequence}, 0, 40) . "\n";
print "Cosing Region string: $dnaSeq{cds}\n";
print "Complement strand   : $dnaSeq{complement}\n\n";

print "getGene()\n";

my %gene = datalayer::getGene($geneIdentifier);
print "Gene				: $geneIdentifier\n";
print "GeneID				: $gene{geneid}\n";
print "Map Location			: $gene{location}\n";
print "Accession			: $gene{accession}\n";
print "Product				: $gene{product}\n";
print "Sequence				: $gene{aa}\n\n";

my %genes = datalayer::getGenes();
my $boredomlimit = 2;
my $testcount = 0;
foreach my $geneIdentifier (keys %genes) {
	print "Gene                : $geneIdentifier\n";
	print "Product		   : $genes{$geneIdentifier}{product}\n";
	print "Map Location	   : $genes{$geneIdentifier}{location}\n";
	print "Accession           : $genes{$geneIdentifier}{accession}\n";
	print "Sequence            : $genes{$geneIdentifier}{aa}\n";
	$testcount ++;
	
	if ($testcount > $boredomlimit) { last; }
}
print "\n\n";

#All done
print "Script complete!\nData is fully loaded\n";
