# This package provides a dual function
# It allows:
# 	- a data extract and load script (datalayer.pl) to parse a chromosomefile and populate a database
# 	- the middle tier package GeneData.pm to call all agreed API functions
#
# The agreed API functions are:
# 	- getGenes();
#	- getGene($);
#	- getDNA($);
#
# These function signatures have been copied from the original location: GeneData.pm where the data was
# originally mocked, and is now real data
#

package datalayer;  

use strict;
use warnings;
#no warnings 'experimental::smartmatch';
use English;
use Exporter;
use Data::Dumper;
our @ISA = ('Exporter'); 
our @EXPORT = ('readConfig','checkDatabaseConnection','checkCommandLineAndFiles','extractDataFromFile','extractCodingRegionExons',
               'extractDNA','extractTranslation', 'extractUntilQuote', 'printChromosomeData','loadDataIntoDatabase','getGenes',
               'getGene','getDNA'); 

use feature "switch";
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use DBI;
use constant { true => 1, false => 0 };

#define %config hash table as global scope
my %config;

#Declarations/prototypes
sub readConfig($);
sub checkDatabaseConnection();
sub checkCommandLineAndFiles();
sub extractDataFromFile($);
sub extractCodingRegionExons($$);
sub extractDNA($);
sub extractTranslation($);
sub extractUntilQuote($$);
sub printChromosomeData(%);
sub loadDataIntoDatabase(%);
sub getGenes();
sub getGene($);
sub getDNA($);
return 1;

# readConfig takes a filename ini file as input and puts the contents into a
# hash map as key value pairs
# it ignores comments and does some validation of mandatory data
#
# returns a string of errors. (empty if none)
#
sub readConfig($)
{
    my $configFilename = $_[0];
	open my $configFileHandle, $configFilename or return "Could not open $configFilename: $!";

	#declare local config hash as return value
	my $errors = '';
	
	while( my $line = <$configFileHandle>)  {   
		#trim both ends (ref: http://perlmaven.com/trim)
		$line =~ s/^\s+|\s+$//g;
		
		#switch case
		given ($line) {
			when (/^[#].*$/) {
				#ignore comments
			}
			when (/^(.*)=(.*)$/)	{ 
				#enfore lowercase keynames
				$config{lc($1)} = $2;
			}
			default {
				$errors .= "Unexpected data in config file. Use hash # to comment out if neccessary: $line";
			}
		}
	}

	close $configFileHandle;
	
	#Do some basic error checking
	if (!defined($config{'driver'})) { 
		$errors .= "The config value key driver is not set in $configFilename.";
	}
	if (!defined($config{'hostname'})) { 
		$errors .= "The config value key hostname is not set in $configFilename.";
	}
	if (!defined($config{'database'})) { 
		$errors .= "The config value key database is not set in $configFilename.";
	}
	if (!defined($config{'user'})) { 
		$errors .= "The config value key user is not set in $configFilename.";
	}
	if (!defined($config{'password'})) { 
		$errors .= "The config value key password is not set in $configFilename.";
	}
	if (length($errors) == 0) {
		#Dynamically build the database dsn now. This will override it if it is set in the config file
		$config{'dsn'} = "DBI:$config{'driver'}:database=$config{'database'};host=$config{'hostname'}";
	}
	
    return $errors;
}

# checkDatabaseConnection simply attempts to connect
# to the mysql database credentials provided in config.ini
#
# returns a string of errors (empty if no errors)
sub checkDatabaseConnection() {
	#Build the DSN and add to config hash.
	
	#Attempt connection
	my $dbh = DBI->connect($config{'dsn'}, 
						   $config{'user'}, 
						   $config{'password'}
	) or return $DBI::errstr;
		
	#Attempt retrieval
	my $id = 1;

	my $sth = $dbh->prepare("SELECT id, externalgeneid
							FROM gene
							WHERE id = ?");
	$sth->execute($id) or return $DBI::errstr;
	#print "Number of rows found: " . $sth->rows . "\n";
	while (my @row = $sth->fetchrow_array()) {
	   my ($id, $externalgeneid ) = @row;
	   #print "id = $id, externalgeneid = $externalgeneid<br>\n";
	}
	$sth->finish();
	
	return '';
}

# The subroutine checkCommandLineAndFiles checks that this script is called
# with minimum parameters and that the file exists and contain text.
#
# If errors exist, it will be implicit by the population of the first
# return parameter.
#
# This subroutine could have returned a boolean primitive to indicate success,
# and set filenames via reference as scalar parameters or as an array.
# Instead, it takes no parameters and returns two scalars, with the first, 
# "$error", to indicate success/failure, allowing the calling program to 
# decide how to proceed.
#
# This is possible because @ARGV and $0 have global scope
sub checkCommandLineAndFiles() {

    if (scalar(@ARGV) != 1) {
        return "\nThis script, $0 requires 1 command-line arguments, and you supplied " . scalar(@ARGV) .
            "\nUsage:\n\t $0 <ChromosomeFile>\n\n";
    }

    #Set input/output files
	my $chromosomefile = $ARGV[0];

    #Ensure input files exist and are text files
    unless (-e $chromosomefile) {
        return "Input filename, \"$chromosomefile\" does not exist";
    }
	
    #T also fails if the file doesn't exist, but separate calls allow me to be more
    #accurate with an error message and also allows the program to short circuit
    unless (-T $chromosomefile) {
			
		eval {
			my ($file, $extension) = split('(\.[^.]+)$', $chromosomefile);
			if (!defined($extension)) { $file = $file . '_uncompressed'; }
		    gunzip $chromosomefile => $file
				or die "gunzip failed: $GunzipError\n";
			
			$chromosomefile = $file;
		};

		if ($@) {
			return "Input filename, \"$chromosomefile\" is not a text file or a .gz extension: $@";
		}
    }    
    return ('', $chromosomefile);
}

# extractDataFromFile takes a filename as input
# It parses the entire chromosome file, creating a heirachical data structure as it goes
# A good example of proof of concept of this approach is provided in an external file
# called  testdataheirachy.pl
#
# It returns a hashtable of all genes
#
sub extractDataFromFile($) {
    my $fileName = $_[0];
	
	my $chromosomeFileHandle;
	my $errors = '';
	
	unless (open($chromosomeFileHandle, "<$fileName")) {
        return "Unable to open input file, $fileName\n";
    }
	
	#Single hasmap to store all relevant chromosome data
	#This variable is to be returned on successful completion of this subroutine
	my %chromosomeData;

	#Variable declaration for locally scoped variables
	my %gene;
	my %codingRegion;
	my %codingRegionExon;
	my $geneCount = 0;
	my $codingRegionCount = 0;
	my $codingRegionExonCount = 0;
	my ($CDSFlag, $productFlag, $translationFlag) = (false, false, false);
	$gene{'mapLocation'} = '';

	#Loop through the file, line by line	   
	while (defined(my $line = <$chromosomeFileHandle>)) {
  
		#trim both ends (ref: http://perlmaven.com/trim)
		$line =~ s/^\s+|\s+$//g;
	
		given ($line) {	
			#Gene Identifier External unique gene identifier from genbank		
			#Note: //VERSION  VERSION     AB026898.1  GI:4835382
			when (/^VERSION[\s]*(\w+[.][\w])[\s]*(GI:[\s\S]*)/) {
				$gene{'geneIdentifier'} = $2;
			}
			
			#Note: FEATURES, source, /map="xxx"
			when (/\/map=\"([\s\S]+)\"/) {
				$gene{'mapLocation'} = $1;
			}
			
			#Note: ACCESSION   AB026898 AB010443
			when (/^ACCESSION[\s]*([\s\S]*)/)	{ 
				$gene{'accession'} = $1;
			}

			#Note: ORIGIN  gttagcggcg (etc.. 269941 characters)
			#In the target data, ORIGIN is always the final section to be parsed
			when (/^ORIGIN/) {
			
				#Slurp up the remaining sequence
				$gene{'nucleotideSequence'} = extractDNA($chromosomeFileHandle);
				
				#Store the number of CDS regions against the locus
				$gene{'numberOfCodingRegions'} = $codingRegionCount;
				
				#Store the entire gene in the chromosome data hash
				%{$chromosomeData{'gene' . $geneCount}} = %gene;
				
				#Increment the gene count for the next locus
				$geneCount ++;
				
				#Empty the container hashes and counts here
				%gene = ();
				#mapLocation is optional
				$gene{'mapLocation'} = '';
				%codingRegion = ();
				%codingRegionExon = ();			
				$codingRegionCount = 0;
				$codingRegionExonCount = 0;
				($CDSFlag, $productFlag, $translationFlag) = (false, false, false);
			}
			
			#Note: Immediately following CDS, the first of 'product' or 'standard_name'
			#Needs error handling here. Not standard names
			#deleted in lung and esophageal cancer 1
			when (/\/product=\"([\s\S]+)/) {
				$codingRegion{'product'} = extractUntilQuote($chromosomeFileHandle, $1);
				$productFlag = true;
			}
			
			when (/\/standard_name=\"([\s\S]+)/) {
				$codingRegion{'product'} = extractUntilQuote($chromosomeFileHandle, $1);
				$productFlag = true;
			}
			
			#Amino-acid sequence
			when (/\/translation=\"(\w+)/) {
				$codingRegion{'aminoAcidSequence'} = $1 . extractTranslation($chromosomeFileHandle);
				$translationFlag = true;
			}
					
			when (/^CDS/) {	
			
				#The CDS is actually pretty tricky, as it can represent in a number of different encoding
				#including plain, with a join, with complement and with 3' or 5' partials
				#Put this functionality in a separate subroutine
				
				($codingRegion{'complement'}, my $exonList) = extractCodingRegionExons($chromosomeFileHandle, $line);
				
				#loop through exonList
				my @exonValues = split(',', $exonList);
				$codingRegionExonCount = 0;
				foreach my $exonValue (@exonValues) {
					my @startStopValues = split('\.\.', $exonValue);
					$codingRegionExon{'sortOrder'} = $codingRegionExonCount;
					$codingRegionExon{'startBase'} = $startStopValues[0];
					$codingRegionExon{'stopBase'} = $startStopValues[1];
					
					#Sometimes, the exon list includes joins to other genes
					#This is not covered by the assessment requirements,
					#So it is being ignored here
					if (!defined($startStopValues[0])) { last; }
					if (!defined($startStopValues[1])) { last; }
					%{$codingRegion{'codingRegionExon' . $codingRegionExonCount}} = %codingRegionExon;
					$codingRegionExonCount ++;					
				}
				# Store number of exons in CDS for looping in order
				$codingRegion{'numberOfExons'} = $codingRegionExonCount;
						
				#Need to increment $codingRegionCount, but cannot commit the
				#codingRegion hashmap to the gene yet.			
				#Because translation and product will not have been set yet.
				$CDSFlag = true;
			}

			#This should never happen, because the nucleotide sequence: "ORIGIN" should slurp the entire locus
			when (/^\/\//) {
				warn "Encountered unexpected token: //";
			}
		}
		
		#If the CDS, product/standard_name, and translation sections have been read
		#Then we can commit the coding region and increment the count
		if ($CDSFlag && $productFlag && $translationFlag) {
			%{$gene{'codingRegion' . $codingRegionCount}} = %codingRegion;
			$codingRegionCount ++;
			($CDSFlag, $productFlag, $translationFlag) = (false, false, false);
		}
		
	}
  
	#Close the file handle
	close($chromosomeFileHandle);
  
  	return ($errors, %chromosomeData);
}


# The CDS region could really beenfit from using a proper lexical parser that could push each open bracket onto a stack... and use recursion to pop off from the stack at the end would:
# Really simplify the job to be done here. Using a proper lexical parsing tree.


# Here are various representative examples of coding region exons within a coding region
# to illustrate how tricky it is
#
# The final example below shows that chromosome 3 has intergene coding regions
=comment

     CDS             1..1068

     CDS             <75..155

     CDS             <1..>127

     CDS             228..>246

     CDS             join(2720..2933,3457..3977)

     CDS             join(<1..122,335..>426)

     CDS             join(2301..2483,5205..5321,6208..6298,7892..8058,
                     9055..9135,11357..11458,13312..14148) 	

     CDS             complement(7683..8132)

     CDS             complement(<150785..150887)

     CDS             complement(join(7708..7898,8308..8461,8546..>8674))

     CDS             complement(join(48019..48168,48898..49063,51954..52113,
                     54578..54782,59250..59412,60258..60499,61546..61656,
                     64804..65031,69052..69143,71126..71266,85338..85458))
					 
	CDS             join(<1445,X77224.1:622..>693)
=cut

# This method generates a data heirachy based on the coding exons
# This subroutine accepts the open chromosome file handle and the already read first line of the CDS block
# Tt returns the complement flag as a binary and a concatenated string of exon locations
sub extractCodingRegionExons($$) {

	#print "extractCodingRegionExons\n";

	my $chromosomeFileHandle = $_[0];
	my $line = $_[1];
	
	#print "line: $line\n";

	#Local boolean variables to flag for close bracket containers
	#Ideally this would be done in some kind of tree/stack like 
	#a real lexical parser... but it seems to only ever be 0, 
	#1, or 2 levels deep  ... e.g. --> "4 + (6 *(3+5))"
	my $complementFlag = false;
	my $joinFlag = false;
	
	#To store a concatenated list of exon start and stop locations
	my $exonList = '';
	
	#First, strip the CDS and any other leading whitespace
	if ($line =~ /[\s]*CDS[\s](.*)$/) {
		$line = $1;
	}
	
	#Check for a complement
	if ($line =~ /complement\((.*)$/) {
		#set complement flag and remove from line
		#Will use boolean flag to account for closing bracket
		$complementFlag = true;
		$line = $1;
	}
	
	#Check for a join
	if ($line =~ /join\((.*)$/) {
		#set complement flag and remove from line
		#Will use boolean flag to account for closing bracket
		$joinFlag = true;
		$line = $1;
	}
	
	#Remove < and > characters because we're not interested in
	# 3' or 5' partials
	$line =~ s/[<>]//g;
	if (!$joinFlag) {
		if ($complementFlag) {
			#scrap trailing brackets
			$line =~ s/[\)]//g;
		}
		#Ready to return
		$exonList = $line;
		return ($complementFlag, $exonList)
	} else {
	
		#If we're a one-liner
		if ($line =~ /\)/) {
			#scrap trailing brackets
			$line =~ s/[\)]//g;
			#Ready to return
			$exonList = $line;
			return ($complementFlag, $exonList)
		}
	}
	
	#Exon list is multi-line
	$exonList = $line;
	while (defined(my $line = <$chromosomeFileHandle>)) {
	
		#trim both ends (ref: http://perlmaven.com/trim)
		$line =~ s/^\s+|\s+$//g;
		#Remove < and > characters because we're not interested in
		# 3' or 5' partials
		$line =~ s/[<>]//g;
		
		#If we are parsing a line containing a double quote, 
		# we know we have hit the final line of the translation
		if ($line =~ /\)/) {
			#scrap trailing brackets
			$line =~ s/[\)]//g;
			$exonList .= $line;
			return ($complementFlag, $exonList);
		}
		$exonList .= $line;
	}
	
	#NB: This code should never be run
	return ($complementFlag, $exonList);
}

# ExtractDNA skurps the nucleotide bases until the locus terminator token is found
# It accepts the open chromosome file handle and retruens the DNS sequence
sub extractDNA($) {
	my $chromosomeFileHandle = $_[0];
	my $nucleotideSequence = '';	
	
	while (defined(my $line = <$chromosomeFileHandle>)) {
	
		#If we are parsing the End-of-locus identifier then break the while loop
		if ($line =~ /^\/\//) {
			#http://perldoc.perl.org/functions/last.html
			last;
		}
		
		#Remove all non base characters	
		$line =~ s/[^acgt]+//g;
		$nucleotideSequence .= $line;
	}
	
	#Return the fully extracted nucleotide sequence
	return $nucleotideSequence;
}

# extractTranslation accepts an open file handle parameter
# it returns an amino acid sequence
sub extractTranslation($) {
	my $chromosomeFileHandle = $_[0];
	my $aminoacidsequence = '';
	

	while (defined(my $line = <$chromosomeFileHandle>)) {
	
		#trim both ends (ref: http://perlmaven.com/trim)
		$line =~ s/^\s+|\s+$//g;
		
		#While there are only ~20-23 amino acids, it is safe enough to include any upper case alpha.
		#This would handle any potential placeholder amino acids. e.g. 'X' etc..
		if ($line =~ /([A-Z]+)/g) {
			$aminoacidsequence .= $1;
		}
		
		#If we are parsing a line containing a double quote, 
		# we know we have hit the final line of the translation
		if ($line =~ /\"/) {
			last;
		}
	}
	return $aminoacidsequence;
}

# extractUntilQuote takes an openchromosome file handle and the already read line of the quoted string as input
# it returns the full string
sub extractUntilQuote($$) {
	my $chromosomeFileHandle = $_[0];
	my $line = $_[1];
	my $stringdata = '';

	if ($line =~ /\"/) {
		$line =~ s/^\s+|\s+$//g;
		$line =~ s/\"//g;
		return $line;
	}
	$stringdata .= $line;
	while (defined(my $line = <$chromosomeFileHandle>)) {
	
		#trim both ends (ref: http://perlmaven.com/trim)
		$line =~ s/^\s+|\s+$//g;
		
		#If we are parsing a line containing a double quote, 
		# we know we have hit the final line of the translation
		if ($line =~ /\"/) {
			$line =~ s/\"//g;
			$stringdata .= $line;
			last;
		} else {
			$stringdata .= $line . " ";
		}
	}
	return $stringdata;
}

# This is a test subroutine to display to the screen a complete chromosome file
# It accepts a hashtable of the entire chromosome
sub printChromosomeData(%) {

	my %chromosomeData = %{$_[0]};
	
	my %gene;
	my %codingRegion;
	my %codingRegionExon;
	my $geneCount = keys %chromosomeData;
	my $codingRegionCount = 0;
	my $codingRegionExonCount = 0;

	print "\n\nStart\n\n";

	for (my $i = 0; $i < $geneCount; $i++) {
		%gene = %{$chromosomeData{'gene' . $i}};
		
		print 'geneIdentifier: ' . $gene{'geneIdentifier'} . "\n";
		print 'mapLocation: ' . $gene{'mapLocation'} . "\n";
		print 'accession: ' . $gene{'accession'} . "\n";
		#print 'nucleotideSequence: ' . $gene{'nucleotideSequence'} . "\n";
		
		print 'products' . "\n";
		my $numberOfCodingRegions = $gene{'numberOfCodingRegions'};
		for (my $j = 0; $j < $numberOfCodingRegions; $j++) {

			%codingRegion = %{$gene{'codingRegion' . $j}};

			print '  | product: ' . $codingRegion{'product'} . "\n";
			print '  | aminoAcidSequence: ' . $codingRegion{'aminoAcidSequence'} . "\n";
			print '  | complement: ' . $codingRegion{'complement'} . "\n";
			print '  | exons' . "\n";

			my $numberOfExons = $codingRegion{'numberOfExons'};
			for (my $k = 0; $k < $numberOfExons; $k++) {
				my %codingRegionExon = %{$codingRegion{'codingRegionExon' . $k}};
				print '      | sortOrder: ' . $codingRegionExon{'sortOrder'} . "\n";
				print '      | startBase: ' . $codingRegionExon{'startBase'} . "\n";
				print '      | stopBase: ' . $codingRegionExon{'stopBase'} . "\n";
			}
		}
	}

	print "\n\nFinish\n\n";
}

# loadDataIntoDatabase accepts a fully loaded chromosome
# and parses the data into a relational database
#
# returns any errors
sub loadDataIntoDatabase(%) {
	my %chromosomeData = %{$_[0]};
	my %gene;
	my %codingRegion;
	my %codingRegionExon;
	my $geneCount = keys %chromosomeData;
	my $codingRegionCount = 0;
	my $codingRegionExonCount = 0;
	
	my $dbh = DBI->connect($config{'dsn'}, 
						   $config{'user'}, 
						   $config{'password'}
	) or return $DBI::errstr;
	
	my $sth;

	for (my $i = 0; $i < $geneCount; $i++) {
		%gene = %{$chromosomeData{'gene' . $i}};
		
		#Insert the gene data
		$sth = $dbh->prepare('INSERT INTO gene (externalgeneid, map, accession) VALUES (?, ?, ? )');
		$sth->execute($gene{'geneIdentifier'}, $gene{'mapLocation'}, $gene{'accession'});
		my $geneid = $sth->{mysql_insertid};
		
		#Insert the associated nucleotide sequence
		$sth = $dbh->prepare('INSERT INTO genedna (geneid, sequence) VALUES (?, ?)');
		$sth->execute($geneid, $gene{'nucleotideSequence'});
		
		my $numberOfCodingRegions = $gene{'numberOfCodingRegions'};
		for (my $j = 0; $j < $numberOfCodingRegions; $j++) {

			%codingRegion = %{$gene{'codingRegion' . $j}};

			#Insert the assocaited CodingRegion
			$sth = $dbh->prepare('INSERT INTO codingregion (geneid, product, sequence, complement) VALUES (?, ?, ?, ?)');
			$sth->execute($geneid, $codingRegion{'product'}, $codingRegion{'aminoAcidSequence'}, $codingRegion{'complement'});
			my $codingregionid = $sth->{mysql_insertid};
			
			my $numberOfExons = $codingRegion{'numberOfExons'};
			for (my $k = 0; $k < $numberOfExons; $k++) {
				my %codingRegionExon = %{$codingRegion{'codingRegionExon' . $k}};
				
				#Insert the associated CodingRegionExon
				$sth = $dbh->prepare('INSERT INTO codingregionexon (codingregionid, sortorder, startbase, stopbase) VALUES (?, ?, ?, ?)');
				$sth->execute($codingregionid, $codingRegionExon{'sortOrder'}, $codingRegionExon{'startBase'}, $codingRegionExon{'stopBase'});
			}
		}
	}
	return '';
}


# Agreed API
# the chromosome data hashtable fully reflects the data heirachy present in the chromosome file
# The database ALSO fully reflects the data heirachy present in the chromosome file
# However, the middle tier and agreed API enforce a one to one mapping between gene and coding region
# This results in this subroutine having to do some 'creative' sql
#
# It uses the limit keyword to flatten the data structure and just take the first coding region
#
# It returns all genes
#
# The agreed API has no error handling
#
sub getGenes() {
	my %genes;
	
	#Attempt connection
	my $dbh = DBI->connect($config{'dsn'}, 
	   $config{'user'}, 
	   $config{'password'}
	) or return $DBI::errstr;
	
	#Prepare a SQL statement to return flat gene data. One coding region per gene
	my $sth;
	$sth = $dbh->prepare("select g.externalgeneid, g.map, g.accession, 
				IFNULL((select product FROM codingregion cr1 where cr1.geneid = g.id limit 1), '') as product,
				IFNULL((select sequence FROM codingregion cr2 where cr2.geneid = g.id limit 1), '') as sequence
				FROM gene g ");
	
	$sth->execute() or return $DBI::errstr;
	
	while (my @row = $sth->fetchrow_array()) {
		my ($geneid, $map, $accession, $product, $sequence) = @row;
		$genes{$geneid}{product} = $product;
		$genes{$geneid}{location} = $map;
		$genes{$geneid}{accession} = $accession;
		$genes{$geneid}{aa} = $sequence;
	}
	$sth->finish();
	
	return %genes;
}

# Agreed API
# The agreed API contains no error handling
# It needs to do some tricky, extra SQL to flatten the data
sub getGene($) {
	my $geneIdentifier = $_[0];
	
	#Attempt connection
	my $dbh = DBI->connect($config{'dsn'}, 
						   $config{'user'}, 
						   $config{'password'}
	) or return $DBI::errstr;
	
	my $sth;
	$sth = $dbh->prepare("SELECT id, map, accession
						FROM gene
						WHERE externalgeneid = ?");
	$sth->execute($geneIdentifier) or return $DBI::errstr;
	#print "Number of rows found: " . $sth->rows . "\n";
	my ($id, $map, $accession) = $sth->fetchrow_array();
	$sth->finish();
	
	$sth = $dbh->prepare("SELECT product, sequence
						FROM codingregion
						WHERE geneid = ?
						limit 1");
	$sth->execute($id) or return $DBI::errstr;
	#print "Number of rows found: " . $sth->rows . "\n";
	my ($product, $sequence) = $sth->fetchrow_array();
	$sth->finish();
	
	#As per agreed API
	my %gene;

	#I know it's weird to return a parameter, but that's what the API says
	$gene{geneid} = $geneIdentifier;
	$gene{location} = $map;
	$gene{accession} = $accession;
	$gene{product} = $product;
	$gene{aa} = $sequence;
	return %gene;
}

# Agreed API
# getDNA accepts a geneIdentifier
#
# It returns a dna sequence, a flattened CDS region for the gene
# and whether the coding region is backwards or not
sub getDNA($) {
	my $geneIdentifier = $_[0];
	
	my $geneid = '';
	my $sequence = '';
	my $codingregionid = '';
	my $complement = '';
	my $cds = '';
	
	#Attempt connection
	my $dbh = DBI->connect($config{'dsn'}, 
						   $config{'user'}, 
						   $config{'password'}
	) or return $DBI::errstr;
	
	my $sth;
	$sth = $dbh->prepare("SELECT g.id, d.sequence
						FROM genedna d
						INNER JOIN gene g ON d.geneid = g.id
						WHERE g.externalgeneid = ?");
	$sth->execute($geneIdentifier) or return $DBI::errstr;
	#print "Number of rows found: " . $sth->rows . "\n";
	($geneid, $sequence) = $sth->fetchrow_array();
	$sth->finish();
	
	#Get one-to-one CDS (limit 1)
	$sth = $dbh->prepare("SELECT id, complement
						FROM codingregion cr
						WHERE cr.geneid = ?
						limit 1");
	$sth->execute($geneid) or return $DBI::errstr;							
	($codingregionid, $complement) = $sth->fetchrow_array();
	$sth->finish();
	
	
	$sth = $dbh->prepare("SELECT startbase, stopbase
						FROM codingregionexon
						WHERE codingregionid = ?
						ORDER BY sortorder");
	
	$sth->execute($codingregionid) or return $DBI::errstr;
	
	while (my @row = $sth->fetchrow_array()) {
		my ($startbase, $stopbase ) = @row;
		$cds .= "$startbase..$stopbase,"
	}
	$sth->finish();
	
	
	#As per agreed API
	my %dnaSeq;
	$dnaSeq{sequence} = $sequence;
	$dnaSeq{cds} = $cds;
	$dnaSeq{complement} = $complement;
	return %dnaSeq;
}
