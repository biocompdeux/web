databasename=${USER}
sqlfile=droptables.sql
echo "This script will drop all tables from database schema for user ${USER} on database $databasename."
read -s -p "Please enter your student mysql password: " mysql_password
echo
echo Running script ...
echo
echo mysql -u ${USER} -p -D $databasename $sqlfile
mysql -u ${USER} -p"$mysql_password" -D $databasename < $sqlfile
echo
echo Process complete ...
echo

