@echo off
cls
cd
REM switching to current directory (if executed via associations and not via command line)
echo Changing to %~dp0%
cd %~dp0%
cd
set mysqlpath=E:\wamp\bin\mysql\mysql5.6.17\bin
set databasename=bc2
set sqlfile=schema.sql
echo.
echo Running script ...
echo.
echo %mysqlpath%\mysql -u root -p -D bc2 %sqlfile%
%mysqlpath%\mysql.exe -u root -p -D bc2 < %sqlfile%
echo.
echo Process complete ...
echo.
pause
REM prompt to continue (this is helpful if executed via associations and not via command line)
