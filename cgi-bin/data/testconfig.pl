#!/usr/bin/perl -w
#testconfig.pl
use strict;
use warnings;
#no warnings 'experimental::smartmatch';
use feature "switch";

my $errors = '';	
my %CFG;
    
	
sub readConfig($)
{
    my $configFilename = $_[0];
	open my $filehandle, $configFilename or return "Could not open $configFilename: $!";

	#declare local config hash as return value
	my %config;
	
	while( my $line = <$filehandle>)  {   
		#trim both ends (ref: http://perlmaven.com/trim)
		$line =~ s/^\s+|\s+$//g;
		
		#switch case
		given ($line) {
			when (/^[#].*$/) {
				#ignore comments
			}
			when (/^(.*)=(.*)$/)	{ 
				$config{$1} = $2;
			}
			default {
				$errors .= "Unexpected data in config file. Use hash # to comment out if neccessary: $line";
			}
		}
		
	}

	close $filehandle;
	
    return ('', %config);
}

# Get our configuration information
($errors, %CFG) = readConfig('config.ini');

#If errors exist, stop processing
if (length($errors) > 0) { die($errors); }

#Dynamically build the database dsn now
$CFG{'dsn'} = "DBI:$CFG{'driver'}:database=$CFG{'database'};host=$CFG{'hostname'}";

print 'driver: ' . 		$CFG{'driver'} . "\n";
print 'hostname: ' . 	$CFG{'hostname'} . "\n";
print 'database: ' . 	$CFG{'database'} . "\n";
print 'user: ' . 		$CFG{'user'} . "\n";
print 'password: ' . 	$CFG{'password'} . "\n";
print 'dsn: ' . 		$CFG{'dsn'} . "\n";
