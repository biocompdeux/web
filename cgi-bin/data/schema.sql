-- Main table for storing header information about a Gene Identifier
CREATE TABLE IF NOT EXISTS gene (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY 	COMMENT 'Auto increment integer as primary surrogate key ',
	externalgeneid VARCHAR(12) NOT NULL 		COMMENT 'External unique Gene Identifier from genbank.\nAlways 12 characters.',
	map VARCHAR(128) NOT NULL 					COMMENT 'Free text map location. Multiple sources. (e.g., chromosome number, cytogenetic location, marker name, physical map location) Largest length is 43 characters in chromosome 3.',
	accession VARCHAR(128) NOT NULL 			COMMENT 'List of accession numbers for gene identifiers. Largest accesssion lenth is 62 characters in chromosome 3.'
);
CREATE INDEX GeneExternalGeneID ON gene(externalgeneid ASC);

-- One to one gene extension table for storing the nucleotide sequence.
CREATE TABLE IF NOT EXISTS genedna (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY	COMMENT 'Auto increment integer as primary surrogate key ',
	geneid INT NOT NULL 						COMMENT 'Foreign Key link to gene table. One to one extension table due to large size of nucleotide sequence.',
	sequence MEDIUMTEXT NOT NULL 				COMMENT 'Nucleotide sequence. The maximum length sequence in chromosome 3 is 269727 characters so MEDIUMTEXT has been used (TEXT has 65535 characters in 64kb, mediumtext 16MB, longtext  4GB etc..)'
);

ALTER TABLE genedna
	ADD CONSTRAINT fk_genedna_gene
	FOREIGN KEY (geneid)
	REFERENCES gene(id);
	
CREATE INDEX GeneDNAGeneID ON gene(id ASC);

-- gene one to many to coding region. Stores coding regions for genes within the Gene Identifier
CREATE TABLE IF NOT EXISTS codingregion (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY	COMMENT 'Auto increment integer as primary surrogate key ',
	geneid INT NOT NULL 						COMMENT 'Foreign Key link to gene table. One Gene to Many Coding Regions.',
	product VARCHAR(128) NOT NULL 				COMMENT 'Coding region product. Largest length is 47 characters in chromosome 3.',
	sequence VARCHAR(4000) NOT NULL 			COMMENT 'Amino acid sequence.  The maximum length sequence in chromosome 3 is 3851 characters so a VARCHAR(4000) has been used.',
	complement TINYINT NOT NULL 				COMMENT 'Boolean flag to determine whether the coding region is to be read forwards or backwards. Complement being true defines backwards read.'
);

ALTER TABLE codingregion
	ADD CONSTRAINT fk_codingregion_gene
	FOREIGN KEY (geneid)
	REFERENCES gene(id);

CREATE INDEX CodingRegionGeneID ON codingregion(geneid ASC);

-- coding region one to many to coding region exon. Stores coding region exons for coding regions
CREATE TABLE IF NOT EXISTS codingregionexon (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY	COMMENT 'Auto increment integer as primary surrogate key ',
	codingregionid INT NOT NULL 				COMMENT 'Foreign Key link to coding region table. One coding region to Many Coding Region exons.',
	sortorder INT NOT NULL 						COMMENT 'Sortorder to concatenate exons together',
	startbase INT NOT NULL 						COMMENT 'Nucleotide base to start sequence at',
	stopbase INT NOT NULL 						COMMENT 'Nucleotide base to stop sequence at'
);

ALTER TABLE codingregionexon
	ADD CONSTRAINT fk_codingregionexon_codingregion
	FOREIGN KEY (codingregionid)
	REFERENCES codingregion(id);

CREATE INDEX CodingRegionExonCodingRegionID ON codingregionexon(codingregionid ASC);

	