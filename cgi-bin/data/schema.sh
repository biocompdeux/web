databasename=${USER}
sqlfile=schema.sql
echo "This script will load the database schema for user ${USER} on database $databasename."
read -s -p "Please enter your student mysql password: " mysql_password
echo
echo Running script ...
echo
echo mysql -u ${USER} -p -D $databasename $sqlfile
mysql -u ${USER} -p"$mysql_password" -D $databasename < $sqlfile
echo
echo Process complete ...
echo

